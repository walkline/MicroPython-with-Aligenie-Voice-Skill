<h1 align="center">MicroPython with Aligenie Voice Skill</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

通过天猫精灵语音技能控制 MicroPython ESP32 开发板

简单说，就是在`ESP32 开发板`上运行一个`Web Server`，用于接收语音技能平台（以下简称`平台`）发送过来的请求，经过分析处理执行相应的操作，最后发送反馈内容给平台，通过天猫精灵语音播报反馈

### 计划目标

* [x] 打开电脑，原理是通过`WOL`远程唤醒局域网内的电脑
* [ ] 获取环境情况，比如当前的温湿度情况
* [ ] 控制`开发板`，比如控制继电器开关等
* [x] 打开单元门门禁（私人用途）

### 项目文件说明

* `config.py`：项目用到的一些配置参数（需要自行修改`config.sample.py`文件名）
* `boot.py`：完成`开发板`上电联网操作
* `microWebSrv.py`：提供`Web Server`服务的模块（已集成在 [ESP32 自定义固件](https://gitee.com/walkline/esp32_firmware) 中）
* `utils/wifihandler.py`：Wifi 连接助手模块
* `utils/wol.py`：局域网唤醒模块
* `utils/acs.py`：门禁开启模块
* `utils/log.py`：打印带当前时间的调试信息
* `aligenie/aligenie.py`：Aligenie 类模块，用于解析`平台`请求数据
* `aligenie/*.txt`：语音技能认证文件

### 平台配置说明

> `平台`配置流程可以参考 [天猫精灵语音技能](https://gitee.com/walkline/aligenie_voice_skills) 项目的说明，不再赘述，这里只说重点部分

* 在`平台`新建如下`实体`

	![](./res/images/entity_list.png)

* 分别编辑刚刚新建的`自定义实体`并填写相应实体值，`公共实体`由公共实体库引入
	> 可以使用`批量导入`功能导入实体值，实体值文件位于`/res/csv/`文件夹内

	| 实体名称 | 实体值 |
	| :-: | :-: |
	| 开关 | ![](./res/images/entity_onoff.png) |
	| 电脑种类 | ![](./res/images/entity_computer.png) |

* 再新建 4 个`意图`

	![](./res/images/intent_list.png)

* 分别编辑 4 个新建的`意图`并填写相应的单轮对话
	> 可以使用`批量导入`功能导入单轮对话，单轮对话文件位于`/res/csv/`文件夹内

	| 意图名称 | 单轮例句 | 参数 |
	| :-: | :-: | :-: |
	| 天气查询 | ![](./res/images/corpus_weather.png) | ![](./res/images/params_weather.png) |
	| 远程唤醒 | ![](./res/images/corpus_wol.png) | ![](./res/images/params_wol.png) |

* 将`认证文件`保存到`/aligenie`文件夹内，`认证文件`文件获取路径为：`回复逻辑`→`逻辑集合`→`默认逻辑WEBHOOK`→`详情`

	> `默认逻辑WEBHOOK`列出的是上边创建的`意图`的`意图标识`，选择其中一个下载`认证文件`即可，无需下载多次

	![](./res/images/04.png)

### config.py 配置文件说明

* 参数对应表

	| config.py | 平台 | 说明 |
	| :-: | :-: | :-: |
	| SKILL_NAME | ![](./res/images/01.png) | |
	| INTENT_* | ![](./res/images/02.png) | 对应`意图标识` |
	| ENTITY_* | ![](./res/images/03.png) | 对应`实体标识名` |

* 其它参数说明

	* `WIFI_*`：`开发板`要连接的 Wifi
	* `IS_MULTI_DEVICES`：如果你的账号绑定了多个天猫精灵设备并且它们不在同一个物理位置（公司一个家里一个），为了避免误操作（在家里打开公司的电脑），参数值应设置为`True`
	* `VALID_DEVICE_IDS`：如果`IS_MULTI_DEVICES`设置为`True`，那么还需要填写合法的天猫精灵`设备 ID`
	* `WOL_MAC_LISTS`：需要被远程唤醒的电脑的 MAC 地址列表

### 路由器设置说明

> 如果不方便设置路由器，或者不想使用这个方法的话可以[使用 MQTT 方式](https://gitee.com/walkline/MicroPython-with-Aligenie-Voice-Skill/tree/feature%2Fbigiot/)进行操作

由于每个路由器的管理页面不尽相同，所以这里只列出设置方法，请自行对照

> 路由器需要支持以下功能：
> * MAC 地址绑定
> * DDNS
> * 虚拟服务器（端口转发）

* 在设置路由器之前，先填写`config.py`中`WIFI_*`的参数，然后上传相关文件到`开发板`并重新上电，使`开发板`连接到路由器

	> `开发板`成功联网后板载蓝灯会常亮，120 秒后仍无法连接外网蓝灯会闪烁 10 次并重启开发板，重复联网动作

* 从路由器`设备管理`页面找到`开发板`，绑定它的`MAC 地址`并设置`固定 IP`
* 打开`DDNS 管理`页面，登录 DDNS 账号并记录域名
* 打开`虚拟服务器管理`页面，新建一个虚拟服务器，任意选取一个可用的`外部端口`，内部端口为`80`，内部 IP 填写`开发板`的`固定 IP`

### 这是最后的设置了

> 上边罗里吧嗦说了那些我也是很崩溃的。。。。但是这都是必要操作，没办法省掉的（衰）

* 路由器正确设置完毕后给`开发板`重新上电，观察它获取到的 IP 地址是否为刚刚设置的`固定 IP`
* 回到`平台`的`回复逻辑`页面，在每个`意图标识`详情页里（下载`认证文件`的页面）填写`URL`地址：

	```python
	# ddns-domain：DDNS 域名
	# port：外部端口
	http://ddns-domain:port/skill
	```

	> 如果想查看`开发板`运行状态，可以用浏览器访问 [http://ddns-domain:port]()，如果网页正常打开说明`开发板`运行正常

* 点击`提交`按钮，如果操作无误的话会提示`保存成功`

* 对天猫精灵说：“天猫精灵，小助手”，观察`REPL`输出内容，`[device]`后边的内容即为天猫精灵`设备 ID`，将`设备 ID`斜杠之前的内容复制到`config.py`的`VALID_DEVICE_IDS`列表中 **（实际应用中发现这个设备 ID后半段会改变，所以只能截取斜杠之前不变的部分）**，如：

	```python
	VALID_DEVICE_IDS = ['9N6LQX6msXOdq']
	```

完整的`REPL`输出内容如下

```doc
>>> Connected
>>> ('192.168.0.240', '255.255.255.0', '192.168.0.1', '192.168.0.1')
>>> Web Server Started
>>> [2020-07-23 17:31:26] [device]: 9N6LQX6msXOdq/xxxxxxxxxx, [skill]: 小助手, [intent]: weather, [ut]: 小助手
```

### 如何使用语音技能

唤醒天猫精灵后可以这样说

```python
# 天气相关的技能并没实现，只是作为测试语料之用
“小助手”
“小助手，今天天气”
“小助手，北京天气”
“小助手，查询上海明天天气”

# 要唤醒局域网内的电脑，需要在 config.py 文件中的 WOL_MAC_LISTS 列表中设置电脑网卡 MAC 地址
# MAC 地址为 12~17 位字符串，分隔符可以使用任意英文字符
“小助手，打开我的电脑”
“小助手，服务器开机”
“小助手，开启台式机”
```

### 合作及交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=VlT7Bjs9)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=IQh2OLw9)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
