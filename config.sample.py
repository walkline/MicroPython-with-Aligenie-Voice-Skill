"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/MicroPython-with-Aligenie-Voice-Skill
"""
class Config(object):
	WIFI_SSID = ""
	WIFI_PASSWORD = ""

	"""
	Web Server 相关
	"""
	SERVER_ROOT = "/"
	SERVER_PORT = 80
	SERVER_SKILL_ROUTE = "/skill"

	"""
	语音技能相关
	"""
	SKILL_NAME = "小助手"

	"""
	意图相关
	"""
	INTENT_WEATHER = "weather" # 天气查询
	INTENT_WOL = "wol" # 远程唤醒
	INTENT_ENVIRONMENT = "environment" # 环境质量
	INTENT_CONTROL = "control" # 设备控制
	INTENT_ACS = "acs" # 门禁控制（个人用途，不通用）

	"""
	实体相关
	"""
	ENTITY_SYS_LOCATION = "sys.location"
	ENTITY_SYS_DATE = "sys.date"
	ENTITY_COMPUTER = "computer"
	ENTITY_ONOFF = "on/off"

	"""
	天猫精灵硬件相关
	"""
	IS_MULTI_DEVICES = False # 一个天猫精灵 APP 账号是否绑定多台天猫精灵设备
	VALID_DEVICE_IDS = [] # 可以控制本设备的天猫精灵设备 ID 列表

	"""
	WOL 相关
	"""
	WOL_MAC_LISTS = {
		"电脑": "",
		"服务器": "",
		"台式机": ""
	}

	"""
	ACS 意图相关
	"""
	ACS_URL = ""

	ACS_DATA = {
		"kind": "",
		"memberid": "",
		"phone": ""
	}

	ACS_HEADERS = {
		"User-Agent": ""
	}
