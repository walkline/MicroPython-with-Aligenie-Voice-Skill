"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/MicroPython-with-Aligenie-Voice-Skill/
"""
from utime import localtime


def logger(msg):
	print("[%02d-%02d-%02d %02d:%02d:%02d] %s" % ((localtime()[:-2]) + (msg,)))
