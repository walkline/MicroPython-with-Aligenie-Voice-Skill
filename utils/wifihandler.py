"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/MicroPython-with-Aligenie-Voice-Skill
"""
import network
import urequests


__station_status_message = {
	network.STAT_IDLE: "network idle",
	# network.STAT_CONNECT_FAIL: "network connect failed",
	network.STAT_CONNECTING: "",
	network.STAT_GOT_IP: "Connected",
	network.STAT_NO_AP_FOUND: "could not found ap",
	network.STAT_WRONG_PASSWORD: "wrong password given",
	network.STAT_BEACON_TIMEOUT: "beacon timeout",
	network.STAT_ASSOC_FAIL: "assoc fail",
	network.STAT_HANDSHAKE_TIMEOUT: "handshake timeout"
}


class WifiHandler(object):
	AP_MODE = 0
	STA_MODE = 1
	STATION_CONNECTED = network.STAT_GOT_IP
	INTERNET_TESTING_URL = "https://gitee.com/walkline/WeatherStation/raw/master/success.html"

	def __init__(self):
		pass

	@staticmethod
	def set_sta_status(active: bool):
		station = network.WLAN(network.STA_IF)
		station.active(active)

	@staticmethod
	def set_ap_status(active: bool):
		access_point = network.WLAN(network.AP_IF)
		access_point.active(active)

	@staticmethod
	def get_sta_status():
		station = network.WLAN(network.STA_IF)

		return station.isconnected()

	@staticmethod
	def set_sta_mode(essid, password, timeout_sec=120):
		from utime import sleep_ms

		station = network.WLAN(network.STA_IF)

		print("\nConnecting to network...")

		if not station.isconnected():
			station.active(True)
			station.connect(essid, password)
			# station.config(dhcp_hostname="esp32")

			retry_count = 0
			while not station.isconnected():
				if timeout_sec > 0:
					if retry_count >= timeout_sec * 2:
						break

				result_code = station.status()

				if result_code == network.STAT_IDLE or\
					result_code == network.STAT_GOT_IP or\
					result_code == network.STAT_NO_AP_FOUND or\
					result_code == network.STAT_WRONG_PASSWORD:
					break
				elif result_code == network.STAT_CONNECTING:
					pass

				retry_count += 1
				sleep_ms(500)

		status_code = station.status()

		print(__station_status_message[status_code])
		print(station.ifconfig())

		return status_code

	@staticmethod
	def check_internet_connection():
		status = False

		try:
			res = urequests.get(WifiHandler.INTERNET_TESTING_URL, timeout=4.0)

			if res:
				if res.text == "Success":
					status = True
		except:
			pass

		return status

	@staticmethod
	def gateway():
		import network

		gateway = None
		station = network.WLAN(network.STA_IF)

		if station.status() == network.STAT_GOT_IP:
			gateway = station.ifconfig()[2]

		return gateway
