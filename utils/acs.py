"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/MicroPython-with-Aligenie-Voice-Skill
"""
import urequests
import json
from config import Config
from utils import log


def open_the_door():
	response = urequests.post(
		url=Config.ACS_URL,
		json=Config.ACS_DATA,
		headers=Config.ACS_HEADERS
	)

	log.logger("[{}, {}] content: {}".format(response.status_code, response.reason, response.content))


if __name__ == "__main__":
	open_the_door()
